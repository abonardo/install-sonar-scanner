#!/bin/bash

cd /tmp || exit
echo "Downloading sonar-scanner......."

if [ -d "/tmp/sonar-scanner-cli-5.0.1.3006-linux.zip" ];then
    sudo rm /tmp/sonar-scanner-cli-5.0.1.3006-linux.zip
fi
wget -q https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-5.0.1.3006-linux.zip
echo "Download completed."
echo

echo "Unziping downloaded file......."
unzip sonar-scanner-cli-5.0.1.3006-linux.zip
echo "Unzip completed."
echo
rm sonar-scanner-cli-5.0.1.3006-linux.zip
echo "Remove .zip"


echo "Installing to opt......."
if [ -d "/var/opt/sonar-scanner-5.0.1.3006-linux" ];then
    sudo rm -rf /var/opt/sonar-scanner-5.0.1.3006-linux
fi
sudo mv sonar-scanner-5.0.1.3006-linux /var/opt

echo "Installation completed successfully."
echo "####################################"
echo "# You can use sonar-scanner!!!!!!! # "
echo "####################################"

sonar_scanner_path='/var/opt/sonar-scanner-5.0.1.3006-linux/bin'
bashrc_path=~/.bashrc

if ! grep -qF "$sonar_scanner_path" "$bashrc_path"; then
    # Se agrega la linea al archivo .bashrc
    echo "$sonar_scanner_path" >> "$bashrc_path"
    echo "Línea de código agregada exitosamente al archivo $bashrc_path."
else
    echo "La línea de código ya existe en el archivo. $bashrc_path."
fi
echo

sonar-scanner -v